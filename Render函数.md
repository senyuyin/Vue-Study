# Render 函数

## 什么是Virtual Dom？

在状态发生变化时，Virtual Dom会进行Diff运算，来更新只需要被替换的DOM，而不是重绘；

## 什么是Render函数？

Render函数通过createElement参数创建Virtual Dom；

## createElement用法

### 基本参数

第一个参数是必选，可以是一个HTML标签，也可以是一个组件或函数；
第二个是可选参数，数据对象，在template中使用；
第三个是子节点，也是可选参数，用法一致。

案例：[checked]

### 约束

如果VNode是组件或含有组件的slot，那么VNode必须唯一；

案例：实验证明这二种情况下，VNode是可以重复渲染的；[checked]

### 使用JavaScript代替模板功能

案例：

* 使用原生JavaScript代替v-if和v-else;[checked]
* 使用原生JavaScript代替v-for;[checked]
* 使用原生JavaScript代替v-model;[checked] v-model是props:value 和 event:input 的组合;
* 模拟聊天室;[checked]
* 没有使用slot时，会显示一个默认的内容;[checked] this.$slots.default来访问组件的默认slot;

## 函数化组件

Vue.js提供了一个functional的布尔值选项，设置为true可以使组件无状态和无实例，也就是没有data和this上下文;

案例：一个根据数据智能选择不同组件的场景；[checked]

## JSX

JSX概念：
    1、JSX是一种看起来像是HTML，但实际上是JavaScript的语法扩展，它用更接近DOM结构的形式来描述一个组件的UI和状态信息；
    2、需要在webpack里配置插件babel-plugin-transform-vue-jsx编译后才可以；

## 实战

    1、使用Render函数开发可排序的表格组件；
    需求解读：
        1、表格的某一个列可以根据选择是递增或递减来进行数据排序；[checked]
        2、表格中的头部可以通过外界输入参数来进行配置；[checked]
        3、表格中的所有数据内容可以通过外界输入数据来进行配置；[checked]
        4、排序可以根据具体的数据类型进行排序，而不是仅限于字符串排序；[checked]
    编码：[checked]
    测试：
        1、对年龄可以递增和递减排序；对出生日期可以递增和递减排序；
        2、添加表头数据：性别和关联数据；
    总结：
        1、什么是表格？
        表格是具有表头和数据的集合；可以对某一列进行排序；

    练习：
        1、给表格添加一个可以设置列宽的width字段，并实现该功能；[checked]
        2、将该实例的Render函数写法改写成Template写法，加以对比，总结出二者之间的差异性，深刻理解其使用场景。
    2、留言列表；
    需求解读：
    编码：
    测试：
    总结：

## 总结